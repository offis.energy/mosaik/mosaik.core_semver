import datetime

from setuptools import setup, find_packages

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')

setup(
    name='mosaik.Core_SemVer',
    version='2.5.3' + 'rc' + TIMESTAMP,
    author='Stefan Scherfke',
    author_email='mosaik@offis.de',
    description='Mosaik is a flexible Smart-Grid co-simulation framework.',
    long_description=(open('README.rst', encoding='utf-8').read() + '\n\n' +
                      open('CHANGES.txt', encoding='utf-8').read() + '\n\n' +
                      open('AUTHORS.txt', encoding='utf-8').read()),
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    url='https://mosaik.offis.de',
    install_requires=[
        'networkx>=2.4',
        'mosaik.API-SemVer>=2.4.2rc20190716091443',
        'mosaik.Core-SemVer>=2.5.2rc20190715231038',
        'simpy>=3.0.10',
        'simpy.io>=0.2.3',
    ],
    packages=find_packages(exclude=["test", "tests"]),
    include_package_data=True,
    entry_points={
        'console_scripts': [
        ],
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'License :: OSI Approved :: GNU Lesser General Public License v2 '
        '(LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
