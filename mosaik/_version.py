"""
This module provides convenient access to the version.
"""

VERSION_INFO = (2, 5, 2)
VERSION = '.'.join(map(str, VERSION_INFO))
