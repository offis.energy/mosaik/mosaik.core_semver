import mosaik_api

from docs.tutorials.code.controller_simulator import ControllerSimulator


def main():
    return mosaik_api.start_simulation(ControllerSimulator())


if __name__ == '__main__':
    main()
