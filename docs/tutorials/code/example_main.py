import mosaik_api

from docs.tutorials.code.example_simulator import ExampleSimulator


def main():
    return mosaik_api.start_simulation(ExampleSimulator())


if __name__ == '__main__':
    main()
